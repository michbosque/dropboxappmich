//
//  UserCustomTableViewCell.h
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCustomTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;


@end
