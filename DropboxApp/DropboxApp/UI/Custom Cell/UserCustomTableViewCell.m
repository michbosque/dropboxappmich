//
//  UserCustomTableViewCell.m
//  DropboxApp
//
//  Created by Bosque, Michelle L. S. on 23/02/2015.
//  Copyright (c) 2015 Accenture. All rights reserved.
//

#import "UserCustomTableViewCell.h"

const int IMAGE_DIMENSION = 80;
const int OFFSET = 10;
const int FONT_TITLE = 13;
const int FONT_DESCRIPTION = 11;

@implementation UserCustomTableViewCell
@synthesize titleLabel;
@synthesize descriptionLabel;
@synthesize imgView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self loadInterface];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadInterface {
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width, 20)];
    titleLabel.textColor = [UIColor blueColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:FONT_TITLE];
    [self addSubview:titleLabel];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - IMAGE_DIMENSION - OFFSET, titleLabel.frame.origin.x + titleLabel.frame.size.height, IMAGE_DIMENSION, IMAGE_DIMENSION)];
    [self addSubview:imgView];
    
    descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, titleLabel.frame.origin.x + titleLabel.frame.size.height, self.frame.size.width - IMAGE_DIMENSION - (OFFSET*2), 100)];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.font = [UIFont systemFontOfSize:FONT_DESCRIPTION];
    [self addSubview:descriptionLabel];
    
}

- (void)dealloc {
    [super dealloc];
    
    [titleLabel release];
    [descriptionLabel release];
}
@end
